package com.example.noticias.services

import com.example.noticias.NewsResponse
import retrofit2.Call
import retrofit2.http.GET

interface APIService {
    @GET("v2/everything?q=bitcoin&from=2021-03-30&sortBy=publishedAt&apiKey=de951dec4fe24a86887f59301d1545b4")
    fun getNewsByCategory(): Call<NewsResponse>
}