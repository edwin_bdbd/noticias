package com.example.noticias.fragments

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.noticias.Articulos
import com.example.noticias.NewsResponse
import com.example.noticias.NoticiasAdapter
import com.example.noticias.R
import com.example.noticias.services.APIServicesDeportes
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [DeportesFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class DeportesFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    private lateinit var adapter: NoticiasAdapter
    private val news = ArrayList<Articulos>()
    private var progressBar: ProgressBar? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        adapter = NoticiasAdapter(news)
        val view = inflater.inflate(R.layout.fragment_deportes, container, false)
        val recyclerView = view.findViewById(R.id.recyclerView_Noticias) as RecyclerView
        progressBar = view.findViewById<ProgressBar>(R.id.progress_Bar) as ProgressBar
        //adding a layoutmanager
        recyclerView.layoutManager = LinearLayoutManager(view.context, LinearLayoutManager.VERTICAL, false)
        recyclerView.adapter = adapter
        if(isNetworkAvailable(view.context)) {
            searchNews()
        }else{
            showError()
        }
        return view
    }
    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder().baseUrl("https://newsapi.org/").addConverterFactory(
            GsonConverterFactory.create()).build()
    }

    private fun searchNews(){
        CoroutineScope(Dispatchers.IO).launch {
            val call: Call<NewsResponse> = getRetrofit().create(APIServicesDeportes::class.java).getNewsByCategory()
            val noticia: NewsResponse? = call.execute().body()
            runOnUiThread {

                //show recyclerview
                val articulos:List<Articulos> = noticia?.data ?: emptyList()
                news.clear()
                news.addAll(articulos)
                progressBar?.setVisibility(View.GONE);
                adapter.notifyDataSetChanged()

            }
        }
    }

    fun Fragment?.runOnUiThread(action: () -> Unit) {
        this ?: return
        if (!isAdded) return // Fragment not attached to an Activity
        activity?.runOnUiThread(action)
    }

    fun isNetworkAvailable(context: Context): Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        var activeNetworkInfo: NetworkInfo? = null
        activeNetworkInfo = cm.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting
    }

    private fun showError() {
        Toast.makeText(this.context, "Por favor validar que tenga salida a internet.", Toast.LENGTH_SHORT).show()
    }

    @Override
    override fun onResume() {
        super.onResume()

        if(isNetworkAvailable(requireContext())) {
            searchNews()
        }else{
            showError()
        }
    }

    override fun onPause() {
        Log.e("DEBUG", "OnPause of HomeFragment")
        super.onPause()
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment DeportesFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            DeportesFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}