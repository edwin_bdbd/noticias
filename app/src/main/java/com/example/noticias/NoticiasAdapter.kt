package com.example.noticias

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso

class NoticiasAdapter(val noticiasList: ArrayList<Articulos>) : RecyclerView.Adapter<NoticiasAdapter.ViewHolder>() {
    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoticiasAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.itemlist_noticias, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: NoticiasAdapter.ViewHolder, position: Int) {
        holder.bindItems(noticiasList[position], position)
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return noticiasList.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(noticia: Articulos, position: Int) {
            val imagen = itemView.findViewById(R.id.imageView_noticia) as ImageView
            val textViewName = itemView.findViewById(R.id.textView_titulo) as TextView
            val textViewAddress  = itemView.findViewById(R.id.textView_detalle) as TextView
            Picasso.with(itemView.context).load(noticia.urlToImage).into(imagen);
            textViewName.text = noticia.title
            textViewAddress.text = noticia.description
        }
    }
}