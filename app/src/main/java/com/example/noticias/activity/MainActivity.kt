package com.example.noticias.activity

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.noticias.R
import com.example.noticias.fragments.DeportesFragment
import com.example.noticias.fragments.FarandulaFragment
import com.example.noticias.fragments.NoticiasFragment
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        // Tabs Customization
        tab_layout.setSelectedTabIndicatorColor(Color.WHITE)
        tab_layout.setBackgroundColor(ContextCompat.getColor(this,
            R.color.colorPrimaryDark
        ))
        tab_layout.tabTextColors = ContextCompat.getColorStateList(this, android.R.color.white)
        // Number Of Tabs
        val numberOfTabs = 3
        tab_layout.tabMode = TabLayout.MODE_FIXED
        tab_layout.isInlineLabel = true
        // Set the ViewPager Adapter
        val adapter = TabsPagerAdapter(
            supportFragmentManager,
            lifecycle,
            numberOfTabs
        )
        tabs_viewpager.adapter = adapter
        // Enable Swipe
        tabs_viewpager.isUserInputEnabled = true

        TabLayoutMediator(tab_layout, tabs_viewpager) { tab, position ->
            when (position) {
                0 -> {
                    tab.text = "Noticias"
                }
                1 -> {
                    tab.text = "Deportes"

                }
                2 -> {
                    tab.text = "Farandula"
                }

            }

        }.attach()
    }
}

class TabsPagerAdapter(fm: FragmentManager, lifecycle: Lifecycle, private var numberOfTabs: Int) : FragmentStateAdapter(fm, lifecycle) {
    override fun createFragment(position: Int): Fragment {
        when (position) {
            0 -> {
                // # Noticias Fragment
                val bundle = Bundle()
                bundle.putString("fragmentName", "Noticias Fragment")
                val noticiasFragment =
                    NoticiasFragment()
                noticiasFragment.arguments = bundle
                return noticiasFragment
            }
            1 -> {
                // # Deportes Fragment
                val bundle = Bundle()
                bundle.putString("fragmentName", "Deportes Fragment")
                val deportesFragment =
                    DeportesFragment()
                deportesFragment.arguments = bundle
                return deportesFragment
            }
            2 -> {
                // # Farandula Fragment
                val bundle = Bundle()
                bundle.putString("fragmentName", "Farandula Fragment")
                val farandulaFragment =
                    FarandulaFragment()
                farandulaFragment.arguments = bundle
                return farandulaFragment
            }
            else -> return NoticiasFragment()
        }
    }

    override fun getItemCount(): Int {
        return numberOfTabs
    }
}
