package com.example.noticias

import android.media.Image

data class Noticias(val image: String, val titulo: String, val description: String)