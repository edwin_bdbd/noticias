package com.example.noticias

import com.google.gson.annotations.SerializedName

data class NewsResponse (
    @SerializedName("status") var estado: String,
    @SerializedName("totalResults") var cantidad: String,
    @SerializedName("articles") var data: ArrayList<Articulos>)

data class Source(
    @SerializedName("id") var id: String,
    @SerializedName("name") var nombre: String
)

data class Articulos(
    @SerializedName("source") var source: Source,
    @SerializedName("author") var author: String,
    @SerializedName("title") var title: String,
    @SerializedName("description") var description: String,
    @SerializedName("url") var url: String,
    @SerializedName("urlToImage") var urlToImage: String,
    @SerializedName("publishedAt") var publishedAt: String,
    @SerializedName("content") var content: String
)